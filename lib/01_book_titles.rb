class Book
  # TODO: your code goes here!
  attr_reader :title

  def title=(title)
    words = title.split(" ").map(&:downcase)
    #map(&:downcase) == map { |word| word.downcase }
    low_case_words = %w[the a an and in of]

    new_title = words.map.with_index do |word, i|
      if low_case_words.include?(word) && i != 0
         word
      else
         word.capitalize
      end
    end

    @title = new_title.join(" ")
  end
end

# Without using @book instead use book = Book.new, it word! HOW?????!!!

class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    hour = seconds / 3600
    min = (seconds / 60) % 60
    second = seconds % 60

    "#{padded(hour)}:#{padded(min)}:#{padded(second)}"
  end

  def padded(num)
    num < 10 ? "0#{num}" : num.to_s
  end
end

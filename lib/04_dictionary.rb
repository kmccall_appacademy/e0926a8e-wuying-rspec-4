class Dictionary
  # TODO: your code goes here!
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(word)
    if word.is_a?(String)
      @entries[word] = nil
    elsif word.is_a?(Hash)
      @entries.merge!(word)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def find(search_word)
    result = {}

    @entries.map do |keys, values|
      if keys.include?(search_word)
        result[keys] = values
      end
    end

    result
  end

  def include?(fragment)
    @entries.keys.include?(fragment)
  end

  def printable
    string = @entries.map do |keys, values|
      %Q{[#{keys}] "#{values}"}
    end

    string.join("\n")
  end

end
